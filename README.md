# Vault

This repository contains the deployment code and configuration for the Vault service. Any change on this repository will be propagated automatically to any Vault instance deployed with [Flux](https://fluxcd.io/) and thus only Vault service managers and automated tools should make changes in this repository.

## Pre-requisites

As this project is aimed to deploy Vault in an existing Kubernetes cluster, its creation must be done beforehand. A recommendation here would be to use a recent version with multi master support.

Another requisite would be a [Helm](https://helm.sh/) binary on the machine from where we plan to deploy, as it will be used to install the different components.

Finally, a Kubernetes secret has to be created with the private SSH key of the vault user in GitLab. The steps to do so can be found [in the official documentation](https://docs.fluxcd.io/en/1.17.0/guides/provide-own-ssh-key.html).

## Deployment

To deploy Vault, we need to install first Helm Operator, which will be the one creating our releases. After that, Flux must be installed and pointed to this repository. The steps to do this are defined in [this repository](https://gitlab.cern.ch/helm/releases/gitops-getting-started).

## Terraform configuration

Vault configuration is applied through a Terraform plan which is executed by a Kubernetes job after any change. After each apply, Terraform will store the state in the Postgres database used by Vault. This will allow to keep track of changes and to remove configuration from the Vault service but removing it in this repository. Any manual change on the Vault service or through any other method will create inconsistencies and thus is discouraged.

